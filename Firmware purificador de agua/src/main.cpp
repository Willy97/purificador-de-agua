#include <Arduino.h>
#include <Servo.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

//Buzzer
#define pinBuzzer 2
//Servos
#define pinServo1 3
#define pinServo2 4
#define pinServo3 5
//Bombas
#define pinBomba1 6
#define pinBomba2 7
#define pinBomba3 8
#define pinBatidor 9
//Botones
#define pinBotonUp A0
#define pinBotonDown A1
#define pinBotonOK A2
#define pinBotonDecline A3

//Posiciones de servos
#define posOpen 90
#define posClose 0

#define apagado HIGH
#define encendido LOW

//Tiempo de filtro de rebote de botones
#define tiemponantirebote 50

Servo servo1;
Servo servo2;
Servo servo3;
int i = 0;
int o = 0;
byte botonData = 0;
int botonDataPos = 0;
bool bandera=false;

byte caraSonriente[8] = {
    B00000,
    B01010,
    B11111,
    B11111,
    B11111,
    B01110,
    B00100,
    B00000};

String menu[4]{"Agua", "Bebida 1", "Bebida 2", "Bebida 3"};

int tiempo = 5;

void setup()
{
  Serial.begin(9600);
  lcd.init(); // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3, 0);
  lcd.print("Hola ");

  // Crear los caracteres personalizados en la pantalla
  lcd.createChar(0, caraSonriente);
  lcd.setCursor(8, 1);

  servo1.attach(pinServo1);
  servo2.attach(pinServo2);
  servo3.attach(pinServo3);

  pinMode(pinBomba1, OUTPUT);
  pinMode(pinBomba2, OUTPUT);
  pinMode(pinBomba3, OUTPUT);
  pinMode(pinBatidor, OUTPUT);
  digitalWrite(pinBomba1, apagado);
  digitalWrite(pinBomba2, apagado);
  digitalWrite(pinBomba2, apagado);
  digitalWrite(pinBatidor, apagado);

  pinMode(pinBotonUp, INPUT_PULLUP);
  pinMode(pinBotonDown, INPUT_PULLUP);
  pinMode(pinBotonOK, INPUT_PULLUP);
  pinMode(pinBotonDecline, INPUT_PULLUP);
}

/*Funcion de manejo de dosificadores*/
void dosificar(byte servo, bool posicion)
{

  switch (servo)
  {
  case 1:
    if (posicion)
    {
      for (int x = 0; x < 90; x++)
      {
        servo1.write(x);
        delay(tiempo);
      }
    }
    else
    {
      for (int x = 90; x > 0; x--)
      {
        servo1.write(x);
        delay(tiempo);
      }
    }
    break;
  case 2:
    if (posicion)
    {
      for (int x = 0; x < 90; x++)
      {
        servo2.write(x);
        delay(tiempo);
      }
    }
    else
    {
      for (int x = 90; x > 0; x--)
      {
        servo2.write(x);
        delay(tiempo);
      }
    }
    break;
  case 3:
    if (posicion)
    {
      for (int x = 0; x < 90; x++)
      {
        servo3.write(x);
        delay(tiempo);
      }
    }
    else
    {
      for (int x = 90; x > 0; x--)
      {
        servo3.write(x);
        delay(tiempo);
      }
    }
    break;

  default:
    break;
  }
}

/*Funcion para leer botones*/
int leerBotones()
{
  int value = 0;
  do
  {
    if (digitalRead(pinBotonUp) == LOW)
      value = 1;
    delay(tiemponantirebote);
    if (digitalRead(pinBotonDown) == LOW)
      value = 2;
    delay(tiemponantirebote);
    if (digitalRead(pinBotonOK) == LOW)
      value = 3;
    delay(tiemponantirebote);
    if (digitalRead(pinBotonDecline) == LOW)
      value = 4;
    delay(tiemponantirebote);
  } while (value == 0);
  return value;
}

/*Funcion para mostrar menu*/
void Menu(int posi)
{
  lcd.clear();

  for (int i = 0; i < 4; i++)
  {
    lcd.setCursor(1, i);
    lcd.print(menu[i]);
  }

  if (posi <= 3 && posi >= 0)
  {
    lcd.setCursor(0, posi);
    lcd.write((byte)0);
  }
}
//Carga agua al vaso
void cargarAgua(int tiempo)
{
  digitalWrite(pinBomba1, encendido);
  delay(tiempo * 1000);
  digitalWrite(pinBomba1, apagado);
}
//Carga agua para realizar mezcla
void cargaAguaParaMezcla(int tiempo)
{
  digitalWrite(pinBomba2, encendido);
  delay(tiempo * 1000);
  digitalWrite(pinBomba2, apagado);
}
//Descarga mezcla al vaso
void descargaMezcla(int tiempo)
{
  digitalWrite(pinBomba3, encendido);
  delay(tiempo * 1000);
  digitalWrite(pinBomba3, apagado);
}

//Funcion batidora
void batir(int tiempo)
{
  digitalWrite(pinBatidor, encendido);
  delay(tiempo * 1000);
  digitalWrite(pinBatidor, apagado);
}

/*Funcion de ejecucion principal*/
void loop()
{

  botonData = leerBotones();
  if (botonData == 1 && botonDataPos < 3)
    botonDataPos++;
  if (botonData == 2 && botonDataPos > 0)
    botonDataPos--;
  Serial.println(botonDataPos);
  Menu(botonDataPos);
  if (botonData == 3)
  {
    if (botonDataPos == 0)
    {
      cargarAgua(200);
      lcd.clear();
      lcd.setCursor(1, 1);
      lcd.print("Preparando Bebida 1");
      cargaAguaParaMezcla(200);
      dosificar(1, true);
      delay(200);
      dosificar(1, false);
      batir(200);
      descargaMezcla(200);
      botonData=0;
      lcd.clear();
      lcd.setCursor(1, 1);
      lcd.print("Finalizando Bebida 1");
    }

    if (botonDataPos == 1)
    {
    }

    if (botonDataPos == 2)
    {
    }

    if (botonDataPos == 3)
    {
    }
  }

}