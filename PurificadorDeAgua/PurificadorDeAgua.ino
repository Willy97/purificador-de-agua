#include <Servo.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

//Buzzer
#define pinBuzzer 2
//Servos
#define pinServo1 3
#define pinServo2 4
#define pinServo3 5
//Bombas
#define pinBomba1 6
#define pinBomba2 7
#define pinBomba3 8
//Botones
#define pinBotonUp A0
#define pinBotonDown A1
#define pinBotonOK A2
#define pinBotonDecline A3

//Posiciones de servos
#define posOpen 90
#define posClose 0

#define apagado HIGH
#define encendido LOW

Servo servo1;
Servo servo2;
Servo servo3;
int i = 0;
int o = 0;
byte caraSonriente[8] = {
  B00000000,
  B01010,
  B11111,
  B11111,
  B11111,
  B01110,
  B00100,
  B00000
};

int tiempo = 5;

void setup() {
  lcd.init();                      // initialize the lcd
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3, 0);
  lcd.print("Hola ");
 
  // Crear los caracteres personalizados en la pantalla
  lcd.createChar (0, caraSonriente);
  lcd.setCursor(8, 1);

  servo1.attach(pinServo1);
  servo2.attach(pinServo2);
  servo3.attach(pinServo3);

  pinMode(pinBomba1, OUTPUT);
  pinMode(pinBomba2, OUTPUT);
  pinMode(pinBomba3, OUTPUT);
  digitalWrite(pinBomba1, apagado);
  digitalWrite(pinBomba2, apagado);
  digitalWrite(pinBomba2, apagado);

  pinMode(pinBotonUp, INPUT_PULLUP);



}

void loop() {
  for ( o = 0; o < 4; o++) {
    lcd.setCursor( i, o);
    for ( i = 0; i < 20; i++) {
      lcd.setCursor( i, o);
      lcd.write((byte)0);
      delay(100);
    }
  }
  while (1);

}

void dosificar(byte servo, bool posicion)
{

  switch (servo)
  {
    case 1:
      if (posicion)
      {
        for (int x = 0; x < 90; x++) {
          servo1.write(x);
          delay(tiempo);
        }
      }
      else
      {
        for (int x = 90; x > 0; x--) {
          servo1.write(x);
          delay(tiempo);
        }
      }
      break;
    case 2:
      if (posicion)
      {
        for (int x = 0; x < 90; x++) {
          servo2.write(x);
          delay(tiempo);
        }
      }
      else
      {
        for (int x = 90; x > 0; x--) {
          servo2.write(x);
          delay(tiempo);
        }
      }
      break;
    case 3:
      if (posicion)
      {
        for (int x = 0; x < 90; x++) {
          servo3.write(x);
          delay(tiempo);
        }
      }
      else
      {
        for (int x = 90; x > 0; x--) {
          servo3.write(x);
          delay(tiempo);
        }
      }
      break;

    default:
      break;
  }
}

void bombasAgua(byte bomba, unsigned int tiempoBomba ) {

  switch (bomba) {
    case 1:
      digitalWrite(pinBomba1, encendido);
      delay(tiempoBomba);
      digitalWrite(pinBomba1, apagado);
      break;
    case 2:
      digitalWrite(pinBomba2, encendido);
      delay(tiempoBomba);
      digitalWrite(pinBomba2, apagado);
      break;
    case 3:
      digitalWrite(pinBomba3, encendido);
      delay(tiempoBomba);
      digitalWrite(pinBomba3, apagado);
      break;
  }

}

